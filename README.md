Code for breadboard 6502 based computer based on [Ben Eater's 6502 project](https://eater.net/6502).  

Contents:
- /arduino 
    - firmware for arduino based eeprom programmer
- /6502
    - Assembly demo program for the computer
    - Bash script to merge and assemble the assembly code
    - Python code to upload the assembled binary to the arduino

![6502 breadboard computer and arduino eeprom programmer](img/6502_01.jpg)
