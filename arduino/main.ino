#define WAITING_FOR_COMMAND 'c'
#define DISABLE_UC          'u'
#define ENABLE_UC           'U'
#define SET_ADDRESS         'a'
#define WRITE_IMMEDIATE     'w'
#define WRITE_16_CHUNK      'k'
#define SET_VECTORS         'v'
#define DUMP_ROM            'r'

#define SHIFT_DATA    3
#define SHIFT_CLK     A0
#define SHIFT_LATCH   4
#define SHIFT_OE      10
#define ROM_WRITE_EN  A5
#define ROM_OUTPUT_EN A4
#define CS_PIN        A1
#define uC_READY_PIN  11


const uint8_t data_pins[8] = {5, 6, 7, 8, 9, A1, A2, A3};
const int startAddress = 0x0000, maxAddress = 0x7FFF;

char state = WAITING_FOR_COMMAND;
byte dataBuffer[16];
int dataIndex = 0;
uint16_t address = 0x0000;
uint16_t fromAddress, toAddress;
char stringBuffer[128];


void setup() {
    digitalWrite(SHIFT_DATA, LOW);  pinMode(SHIFT_DATA, OUTPUT);  
	digitalWrite(SHIFT_CLK, LOW);   pinMode(SHIFT_CLK, OUTPUT);   
	digitalWrite(SHIFT_LATCH, LOW); pinMode(SHIFT_LATCH, OUTPUT); 
	digitalWrite(SHIFT_OE, HIGH);   pinMode(SHIFT_OE, OUTPUT);    

	digitalWrite(ROM_WRITE_EN, HIGH); pinMode(ROM_WRITE_EN, OUTPUT);
	digitalWrite(ROM_OUTPUT_EN, LOW); pinMode(ROM_OUTPUT_EN, OUTPUT);

	pinMode(uC_READY_PIN, INPUT);

    Serial.begin(57600);
    while (!Serial);
}

void loop() {
    while(Serial.available()) {
        int data_read = Serial.read();
        if (data_read < 0) continue;
        byte data = data_read;
        switch (state) {
            case WAITING_FOR_COMMAND:
                if (data == SET_ADDRESS || data == WRITE_IMMEDIATE || data == WRITE_16_CHUNK || data == SET_VECTORS || data == DUMP_ROM) {
                    state = data;
                } else if (data == DISABLE_UC) {
                    Serial.println("Disabling Microcontroller for programming");
	                digitalWrite(uC_READY_PIN, LOW); pinMode(uC_READY_PIN, OUTPUT);
                    digitalWrite(SHIFT_OE, LOW);
                    Serial.println();

                } else if (data == ENABLE_UC) {
                    Serial.println("Re-enabling Microcontroller after programming");
	                digitalWrite(uC_READY_PIN, HIGH); pinMode(uC_READY_PIN, INPUT);
                    digitalWrite(SHIFT_OE, HIGH);
                    Serial.println();
                }
                dataIndex = 0;
                break;

            case SET_ADDRESS:
                dataBuffer[dataIndex++] = data;
                if (dataIndex >= 2) {
                    address = dataBuffer[0];
                    address <<= 8;
                    address += dataBuffer[1];

                    sprintf(stringBuffer, "Setting address 0x%04x", address);
                    Serial.println(stringBuffer);
                    Serial.println();
                    
                    state = WAITING_FOR_COMMAND;
                }
                break;

            case WRITE_IMMEDIATE:
                writeEEPROM(address, data);
                state = WAITING_FOR_COMMAND;
                break;

            case WRITE_16_CHUNK:
                dataBuffer[dataIndex++] = data;
                if (dataIndex >= 16) {
                    sprintf(stringBuffer, "Writing chunk - 0x%04x: ", address);
                    Serial.print(stringBuffer);
                    
                    for(int i = 0; i < 16; i++) {
                        sprintf(stringBuffer, "%02x ", dataBuffer[i]);
                        Serial.print(stringBuffer);
                        writeEEPROM(address, dataBuffer[i]);
                        address++;
                    }
                    Serial.println();
                    state = WAITING_FOR_COMMAND;
                }
                break;

            case SET_VECTORS:
                dataBuffer[dataIndex++] = data;
                if (dataIndex >= 6) {
                    sprintf(stringBuffer, "Setting vectors - NMI: %02x%02x, RESET: %02x%02x, IRQ: %02x%02x\n",
                        dataBuffer[1], dataBuffer[0], dataBuffer[3], dataBuffer[2], dataBuffer[5], dataBuffer[4]);
                    Serial.println(stringBuffer);

                    setAddress(0x7ff0, false);
                    delay(10); // Wait for page change (?)
                    for(uint16_t i = 0x7ff0; i <= 0x7fff; i++) writeEEPROM(i, 0x00);
                    // NMI
                        writeEEPROM(0x7ffa, dataBuffer[0]);
                        writeEEPROM(0x7ffb, dataBuffer[1]);
                    // Reset vector
                        writeEEPROM(0x7ffc, dataBuffer[2]);
                        writeEEPROM(0x7ffd, dataBuffer[3]);
                    // IRQB
                        writeEEPROM(0x7ffe, dataBuffer[4]);
                        writeEEPROM(0x7fff, dataBuffer[5]);

                    state = WAITING_FOR_COMMAND;
                }
                break;

            case DUMP_ROM:
                dataBuffer[dataIndex++] = data;
                if (dataIndex >= 4) {
                    fromAddress = dataBuffer[0];
                    fromAddress <<= 8;
                    fromAddress += dataBuffer[1];

                    toAddress = dataBuffer[2];
                    toAddress <<= 8;
                    toAddress += dataBuffer[3];

                    sprintf(stringBuffer, "\nDumping from address 0x%04x to address 0x%04x", fromAddress, toAddress);
                    Serial.println(stringBuffer);
                    printContents(fromAddress, toAddress);
                    Serial.println("...");
	                printContents(0x7ff0, 0x7fff);
                    Serial.println();
                    state = WAITING_FOR_COMMAND;
                }
                break;
        }
    }
}

/*
 * Output the address bits and outputEnable signal using shift registers.
 */
void setAddress(int address, bool outputEnable) {
	digitalWrite(SHIFT_DATA, HIGH);
	shiftOut(SHIFT_DATA, SHIFT_CLK, MSBFIRST, address >> 8);
	shiftOut(SHIFT_DATA, SHIFT_CLK, MSBFIRST, address);
	digitalWrite(SHIFT_DATA, LOW);

	digitalWrite(ROM_OUTPUT_EN, outputEnable ? LOW : HIGH);

	digitalWrite(SHIFT_LATCH, LOW);
	digitalWrite(SHIFT_LATCH, HIGH);
	digitalWrite(SHIFT_LATCH, LOW);
}

/*
 * Read a byte from the EEPROM at the specified address.
 */
byte readEEPROM(int address) {
	for (int pin = 0; pin <= 7; pin += 1) {
		pinMode(data_pins[pin], INPUT);
	}
	setAddress(address, /*outputEnable*/ true);

	byte data = 0;
	for (int pin = 7; pin >= 0; pin -= 1) {
		data = (data << 1) + digitalRead(data_pins[pin]);
	}
	return data;
}

void writeDataBus(byte data) {
	for (int pin = 0; pin <= 7; pin += 1) {
		pinMode(data_pins[pin], OUTPUT);
	}

	for (int pin = 0; pin <= 7; pin += 1) {
		digitalWrite(data_pins[pin], data & 1);
		data = data >> 1;
	}
}

/*
 * Write a byte to the EEPROM at the specified address.
 */
void writeEEPROM(int address, byte data) {
	setAddress(address, /*outputEnable*/ false);
	writeDataBus(data);

	digitalWrite(ROM_WRITE_EN, LOW);
	delayMicroseconds(1);
	digitalWrite(ROM_WRITE_EN, HIGH);
	delay(11);
	digitalWrite(ROM_OUTPUT_EN, LOW);
}

/*
 * Read the contents of the EEPROM and print them to the serial monitor.
 */
void printContents(uint16_t start, uint16_t end) {
	for (uint16_t base = start; base <= end; base += 16) {
		byte data[16];
		for (int offset = 0; offset <= 15; offset += 1) {
			data[offset] = readEEPROM(base + offset);
		}

		char buf[80];

		sprintf(buf, "0x%04x: %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x",
				base, data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7],
				data[8], data[9], data[10], data[11], data[12], data[13], data[14], data[15]);
		Serial.print(buf);
		Serial.print("  | ");
		
		for (int i = 0; i < 16; i++) {
			if (data[i] < 32 || data[i] > 126) data[i] = ' ';
		}
		sprintf(buf, "%c %c %c %c  %c %c %c %c  %c %c %c %c  %c %c %c %c",
				data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7],
				data[8], data[9], data[10], data[11], data[12], data[13], data[14], data[15]);
		Serial.print(buf);
		Serial.println(" |");

	}
}