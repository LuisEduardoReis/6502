; vasm6502_oldstyle -dotdir -Fbin test.s

PORTB = $6000
PORTA = $6001
DDRB  = $6002
DDRA  = $6003
T1CL  = $6004
T1CH  = $6005
ACR   = $600B
PCR   = $600C
IFR   = $600D
IER   = $600E

; RAM Global Variables
value                  = $0200 ; 2 bytes
mod10                  = $0202 ; 2 bytes
message                = $0204 ; 16 bytes
counter                = $0214 ; 2 bytes
uptime                 = $0216 ; 4 bytes
busy_wait_timer        = $021a ; 1 byte
busy_wait_delay        = $021b ; 1 byte
pattern_data           = $021c ; 16 bytes
lcd_screen_ram_aux     = $022c ; 2 bytes
lcd_screen_ram         = $022e ; 32 bytes
shift_register         = $024e ; 2 bytes
shift_register_aux_bit = $0250 ; 1 byte


; Vector locations read by arduino. starts at $fffa hardware, $7ffa for assembler
  .org $7ffa
  .word nmi   ; FFFA,B Non-Maskable interrupt routine
  .word setup ; FFFC,D Reset Vector
  .word irq   ; FFFE,F Interrupt request routine

  .org $8000
setup:
  lda #%01111111 ; Set top 4 pins on port B to output
  sta DDRB
  
  lda #0 
  sta message ; Clear message
  sta counter ; Reset counter
  sta counter + 1
  sta busy_wait_timer ; Reset busy wait timer
  sta busy_wait_delay ; Reset busy wait delay

  ; Load random RAM value to shift_register
  lda $0456
  sta shift_register
  lda $0613
  sta shift_register + 1

  ; Timer 1
  jsr init_timer_1

  ; CA1 active edge
  lda PCR
  ora #%00000001
  sta PCR
  
  ; CA1 enable
  lda #%10000010
  sta IER

  cli
  jsr lcd_init
  jsr clear_lcd
  
  lda #0
  sta pattern_select
  jsr reset_pattern

  jmp main_loop


main_loop:
  jsr update_pattern
  jsr print_lcd_ram
  jmp main_loop
