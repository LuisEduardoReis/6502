init_timer_1:
  lda #0
  sta uptime    ; Reset uptime
  sta uptime + 1
  sta uptime + 2
  sta uptime + 3

  ;lda ACR
  ;and #%01111111 ; Set T1 Timer control to 01 = Continuous interrupts
  ;ora #%01000000
  lda #%01000000
  sta ACR

  ; 1Ghz / 1ms = 1000 cycles. Extra 2 cycles for timer reset; 998 dec = 0x03e6
  ; 1Ghz / 10ms = 10000 cycles. Extra 2 cycles for timer reset; 9998 dec = 0x270e
  ; 38.5khz / 1ms = 39 cycles. 64 to be safe at low speed. 64 dec = 0x0040
  lda #$e6
  sta T1CL
  lda #$03
  sta T1CH  ; Timer starts with load of high byte
  ;lda #$40
  ;sta T1CL
  ;lda #$00
  ;sta T1CH  ; Timer starts with load of high byte

  lda #%11000000 ;Enable Timer 1 interrupts
  sta IER

  rts

nmi:
  rti

irq:
  pha  
  lda IFR
  and #%01000000
  bne handle_timer

  lda IFR
  and #%00000010
  bne handle_button

  bit T1CL
  bit PORTA
  jmp return_irq

  handle_timer:
    bit T1CL
    jsr inc_uptime
    jmp return_irq

  handle_button:
    bit PORTA
    inc pattern_select
    lda pattern_select
    and #%00000011 ; Mod 4
    sta pattern_select
    lda #$01
    sta pattern_reset_request
    jmp return_irq

  return_irq:
  pla
  rti


; Increment 32 bit uptime var
inc_uptime:
  inc uptime
  bne return_inc_uptime
  inc uptime + 1
  bne return_inc_uptime
  inc uptime + 2
  bne return_inc_uptime
  inc uptime + 3
  return_inc_uptime:
  rts

; Increment 16 bit counter var
inc_counter:
  inc counter
  bne return_inc_counter
  inc counter + 1
  return_inc_counter:
  rts