pattern_0_reset:
  lda #50
  sta pattern_delay
  lda #0
  sta pattern_delay+1
  lda #'!'
  sta pattern_char
  lda #0
  sta pattern_index
  rts


pattern_0_character_listing:
  ; Put the current char in memory
  lda pattern_char  
  ldx pattern_index
  sta lcd_screen_ram,x

  ; Increment the char to run through all of them
  inc pattern_char

  ; Increment the index to the next position in ram, rolling over at 32
  inc pattern_index
  lda pattern_index
  and #31
  sta pattern_index

  rts
