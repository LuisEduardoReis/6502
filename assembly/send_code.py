# Importing Libraries
from struct import pack
import serial
import time
from functools import partial

arduino = serial.Serial(port='/dev/ttyUSB0', baudrate=57600, timeout=1)
time.sleep(1.5)
while not arduino.isOpen():
    pass

def read_arduino_serial_line():
    data = arduino.readline()
    if (len(data) > 0): #eof or empty line
        print(data.decode("utf-8"), end='')
        
def read_arduino_serial_timeout():
    while True:
        data = arduino.read()
        if (len(data) == 0):
            break
        print(data.decode("utf-8"), end='')
        

def send_packet(data):
    packet = bytearray()
    for d in data:
        packet.append(d)
    arduino.write(packet)
    read_arduino_serial_line()


# Fill code array with input file
code = []
with open('a.out', 'rb') as code_file:
    while True:
        data = code_file.read(16)
        if (len(data) == 0):
            break
        for op in data:
            code.append(op)

vectors = code[0:6]
code = code[6:]

# Pad code array if needed
while len(code) % 16 != 0:
    code.append(0)


send_packet([ord('u')])

vectors.insert(0, ord('v'))
send_packet(vectors)

send_packet([ord('a'), 0x00, 0x00])
num_chunks = int(len(code) / 16)
for i in range(0, num_chunks):
    chunk = code[i * 16 : (i+1) * 16]
    chunk.insert(0, ord('k'))
    send_packet(chunk)


to_address_low = (num_chunks * 16 - 1) & 0xff
to_address_high = ((num_chunks * 16 - 1) & 0xff00) >> 8
send_packet([ord('r'), 0x00, 0x00, to_address_high, to_address_low])
send_packet([ord('U')])



time.sleep(0.1)
read_arduino_serial_timeout()
    