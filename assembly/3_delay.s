


; Delay ms - Busy waits for the number of milliseconds on accumulator
; Maximum = 255 ms, Doesn't change A
delay_ms:
  pha
  sta busy_wait_delay
  lda uptime
  sta busy_wait_timer

  delay_ms_loop:
    sec
    lda uptime
    sbc busy_wait_timer
    cmp busy_wait_delay
    bcc delay_ms_loop
    
  pla
  rts

; Delay ms - Busy waits for the number of secs on accumulator
; Maximum = 255 s
delay_sec:
  tax      ; put number of seconds to wait on X
  lda #$fa ; load 250ms to A
  delay_sec_loop:
    jsr delay_ms
    jsr delay_ms
    jsr delay_ms
    jsr delay_ms
    dex
    bne delay_sec_loop
  rts
