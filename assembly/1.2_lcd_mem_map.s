; lcd_screen_ram_aux = $022c ; 2 bytes
; lcd_screen_ram     = $022e ; 32 bytes

lcd_cursor_pos = $022c

clear_lcd:
  ldx #0
  clear_lcd_loop:
    lda #' '
    sta lcd_screen_ram,x
    inx
    cpx #32
    bne clear_lcd_loop
  rts

print_lcd_ram:  
  lda #0
  sta lcd_cursor_pos
  
  ldx #0
  print_lcd_ram_loop:
    ; Set current cursor pos
    lda lcd_cursor_pos
    ora #$80
    jsr lcd_instruction

    ; Print char
    lda lcd_screen_ram,x
    jsr print_lcd_char
    
    ; Increment cursor pos
      inc lcd_cursor_pos
      ; If first 4 bits are 0, it rolled over
      lda lcd_cursor_pos
      and #$0f
      bne print_lcd_ram_loop_dont_roll_over
      ; Roll over cursor
      lda lcd_cursor_pos
      and #$40
      eor #$40
      sta lcd_cursor_pos
      print_lcd_ram_loop_dont_roll_over:
    ;Finish increment cursor post
    
    inx
    cpx #32
    bne print_lcd_ram_loop
  ;end print_lcd_ram_loop
  rts

; Print char - Prints a character at the current cursor pos and then increments it
print_char:
  ldy lcd_cursor_pos
  sta lcd_screen_ram,y
  jsr increment_cursor
  rts

; Print message - Loops through message and calls print_char for each char until null terminator
print_message:
  ldx #0
  print_loop:
    lda message,x
    beq return_print_message
    jsr print_char
    inx
    jmp print_loop
  return_print_message:
    rts

increment_cursor:
  clc
  lda lcd_cursor_pos
  adc #1
  and #31
  sta lcd_cursor_pos
  rts