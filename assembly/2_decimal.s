; Print Value - Prints value onto message as decimal ascii
print_value:
  lda #0
  sta message
  jsr divide_into_message
  jsr print_message
  rts


  ; Divide into message - Fills message with "value"'s representation in ascii decimal
  divide_into_message:
    ; Initialize the remainder to zero
    lda #0
    sta mod10
    sta mod10 + 1
    clc
    ; Initialize X as iterator for digit_loop
    ldx #16

    division_loop:
      ; Rotate quotient and remainder
      rol value
      rol value + 1
      rol mod10
      rol mod10 + 1

      ; a,y = dividend - devisor
      sec ; set carry
      lda mod10
      sbc #10 ; subtract divisor (division iteration)
      tay ; save low byte in Y
      lda mod10 + 1 ; save high byte in A
      sbc #0 ; subtract carry from high byte
      bcc finish_digit ; finish if dividend < devisor (aka finish if dividend - divisor is negative)
      sty mod10
      sta mod10 + 1
      
      finish_digit:
      dex
      bne division_loop

    rol value ; shift in the last bit of the quotient
    rol value + 1

    lda mod10 ; load low byte (which contains decimal digit)
    clc       ; clear carry to do ascii conversion safely
    adc #"0"  ; convert digit to ascii
    jsr push_char

    ; Recursive "call" until value to divide is 0
    lda value
    ora value + 1
    bne divide_into_message

  rts


; Push Char - Puts char into start of message and shifts all other characters right
push_char:
  ; S = carry
  ; y = value 
  tay
  ldx #0
    
  push_char_loop:
    lda message,x ; Get current char to carry
    pha           ; Put it on the stack
    tya           ; Get value from Y
    sta message,x ; Store value on message
    inx           ; Move to next messsage address
    pla           ; Get carried char from stack
    tay           ; And it becomes Y of next iteration
    bne push_char_loop ; If next value is not a null, continue loop
  
  lda #0
  sta message,x   ; Complete with null terminator
  rts