
vector_x = pattern_value_A
vector_y = pattern_value_A + 1
pos_x = pattern_value_B
pos_y = pattern_value_B + 1
prev1 = pattern_value_C
prev2 = pattern_value_C + 1
prev3 = pattern_value_D
size_x = 64
size_y = 8

pattern_2_vector_values: .byte 2,3,4,3, 4,2,3,2



pattern_2_reset:
  lda #200
  sta pattern_delay
  lda #0
  sta pattern_delay+1
  lda #0
  sta pattern_index
  
  ; Start vector is (2,2)
  lda #2
  sta vector_x
  sta vector_y

  ; Start pos is (0,0)
  lda #0
  sta pos_x
  sta pos_y
  sta prev1
  sta prev2
  sta prev3
  rts


pattern_2_ball:

  pattern_2_check_x_coordinates:
    clc
    lda pos_x
    adc vector_x
    sta pos_x
    bmi pattern_2_bounce_x_left
    cmp #size_x
    bcs pattern_2_bounce_x_right
    jmp end_pattern_2_check_x_coordinates

    pattern_2_bounce_x_left:
      jsr pattern_2_get_random_vector_value
      sta vector_x
      sta pos_x
      jmp end_pattern_2_check_x_coordinates

    pattern_2_bounce_x_right:
      jsr pattern_2_get_random_vector_value
      eor #$ff
      sec
      adc #0
      sta vector_x
      adc #size_x - 1
      sta pos_x
  end_pattern_2_check_x_coordinates:

  pattern_2_check_y_coordinates:
    clc
    lda pos_y
    adc vector_y
    sta pos_y
    bmi pattern_2_bounce_y_up
    cmp #size_y
    bcs pattern_2_bounce_y_down
    jmp end_pattern_2_check_y_coordinates

    pattern_2_bounce_y_up:
      lda #3
      sta vector_y
      sta pos_y
      jmp end_pattern_2_check_y_coordinates

    pattern_2_bounce_y_down:
      lda #-3
      sta vector_y
      adc #size_y - 1
      sta pos_y
  end_pattern_2_check_y_coordinates:

  pattern_2_print_ball:
    lda prev3
    sta lcd_cursor_pos
    lda #' ' ; tail
    jsr print_char

    lda prev2
    sta lcd_cursor_pos
    sta prev3    
    lda #%10100101 ; small dot centered
    jsr print_char

    lda prev1
    sta lcd_cursor_pos
    sta prev2
    lda #%10100001 ; medium dot centered
    jsr print_char

    ldx pos_x
    ldy pos_y
    jsr pattern_2_update_lcd_cursor
    sta prev1
    lda #'o'
    jsr print_char
  end_pattern_2_print_ball:

  return_pattern_2_ball:
  rts



pattern_2_get_random_vector_value:
  lda pattern_index
  clc
  adc #1
  and #%111 ; 8 different value
  sta pattern_index
  tax
  lda pattern_2_vector_values,x
  rts

pattern_2_update_lcd_cursor:
    tya
    and #%00000100      ; [0-7] & 4 = (0,4)
    asl
    asl                 ; (0,4) << 2 = (0,16)    
    sta lcd_cursor_pos

    txa
    lsr
    lsr                 ; [0-63] >> 2 = [0-15]
    clc
    adc lcd_cursor_pos  ; [0-15] + (0,16) = [0,31]
    sta lcd_cursor_pos
  rts
