E  = %01000000
RW = %00100000
RS = %00010000
BF = %1000

lcd_init:
  lda #%01111111
  sta DDRB

  lda #100   ; wait 100 ms for power up
  jsr delay_ms

  ; Function set 3 times to signal the lcd we want to reset
  lda #%0011
  jsr lcd_4bit_instruction
  lda #10   ; wait 10 ms for first function set
  jsr delay_ms
  lda #%0011
  jsr lcd_4bit_instruction
  lda #1   ; wait 1 ms for second function set
  jsr delay_ms
  lda #%0011
  jsr lcd_4bit_instruction
  lda #1   ; wait 1 ms for third function set
  jsr delay_ms

  ; Function set to enable 4-bit mode
  lda #%0010
  jsr lcd_4bit_instruction
  lda #1   ; wait 1 ms for 'real' function set
  jsr delay_ms

  ; Setup on 4-bit mode
  lda #%00101000 ; 4-bit mode; 2-line display; 5x8 font
  jsr lcd_instruction
  lda #%00001100 ; Display on; cursor off; blink off
  jsr lcd_instruction
  lda #%00000001 ; Clear display
  jsr lcd_instruction
  lda #%00000110 ; Increment and shift cursor; Don't scroll
  jsr lcd_instruction
  rts


; LCD Instruction - send instruction on A to LCD
lcd_instruction:
  jsr lcd_wait
  pha
  lsr
  lsr
  lsr
  lsr            ; Send high 4 bits
  sta PORTB
  ora #E         ; Set E bit to send instruction
  sta PORTB
  eor #E         ; Clear E bit
  sta PORTB
  pla
  and #%00001111 ; Send low 4 bits
  sta PORTB
  ora #E         ; Set E bit to send instruction
  sta PORTB
  eor #E         ; Clear E bit
  sta PORTB
  rts

lcd_4bit_instruction:
  sta PORTB
  ora #E
  sta PORTB
  eor #E
  sta PORTB
  rts

; LCD Wait - wait for LCD busy flag to be unset
lcd_wait:
  pha
  lda #%01110000  ; LCD data is input
  sta DDRB
  lcdbusy:
    lda #RW
    sta PORTB
    lda #(RW | E)
    sta PORTB
    lda PORTB       ; Read high nibble
    pha             ; and put on stack since it has the busy flag
    lda #RW
    sta PORTB
    lda #(RW | E)
    sta PORTB
    lda PORTB       ; Read low nibble
    pla             ; Get high nibble off stack
    and #BF
    bne lcdbusy

  lda #RW
  sta PORTB
  lda #%01111111  ; LCD data is output
  sta DDRB
  pla
  rts

; Print Char - Print char on LCD
print_lcd_char:
  jsr lcd_wait
  pha
  lsr
  lsr
  lsr
  lsr             ; Send high 4 bits
  ora #RS         ; Set RS
  sta PORTB
  ora #E          ; Set E bit to send instruction
  sta PORTB
  eor #E          ; Clear E bit
  sta PORTB
  pla
  and #%00001111  ; Send low 4 bits
  ora #RS         ; Set RS
  sta PORTB
  ora #E          ; Set E bit to send instruction
  sta PORTB
  eor #E          ; Clear E bit
  sta PORTB
  rts


