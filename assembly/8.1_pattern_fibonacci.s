
pattern_1_message: .asciiz "Fibonacci:"

pattern_1_reset:
  ; Reset fibonacci numbers to 1,1
  lda #1
  sta pattern_value_A
  lda #0
  sta pattern_value_A + 1
  sta pattern_value_B
  sta pattern_value_B + 1
  
  jsr pattern_1_fibbonaci

  ; Setup 750ms delay
  ; dec 750 = 0x02ee
  lda #$ee
  sta pattern_delay
  lda #$02
  sta pattern_delay+1

  rts


pattern_1_fibbonaci:
  jsr clear_lcd

  ; Print message on the first line
  lda #0
  sta lcd_cursor_pos
  ldx #0
  pattern_1_print_message_loop:
    lda pattern_1_message,x
    beq end_pattern_1_print_message_loop
    jsr print_char
    inx
    jmp pattern_1_print_message_loop
  end_pattern_1_print_message_loop:

  ; Print A on the second line
  lda pattern_value_A
  sta value
  lda pattern_value_A+1
  sta value+1  
  lda #16
  sta lcd_cursor_pos
  jsr print_value

  ; Add B onto A
  clc
  lda pattern_value_A
  pha ; push current value of A to move to B later
  adc pattern_value_B
  sta pattern_value_A

  lda pattern_value_A+1
  pha ; push current value of A to move to B later
  adc pattern_value_B+1
  bvs pattern_1_fibbonaci_overflow
  sta pattern_value_A+1

  ; Pop to store prev value of A onto B
  pla
  sta pattern_value_B+1
  pla
  sta pattern_value_B
  jmp return_pattern_1_fibbonaci

  pattern_1_fibbonaci_overflow:
    lda #1
    sta pattern_value_A
    lda #0
    sta pattern_value_A + 1
    sta pattern_value_B
    sta pattern_value_B + 1

  return_pattern_1_fibbonaci:
  rts