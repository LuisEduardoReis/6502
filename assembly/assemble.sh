OUTPUT_FILE='/tmp/combined_assembly.s'
assembly_files=($(ls | grep -e ^[0-9].*\.s$ ))

echo "Merging ${#assembly_files[@]} assembly files:"

echo > $OUTPUT_FILE
for file in "${assembly_files[@]}"; do
    echo "  - $file"
    cat $file >> $OUTPUT_FILE
    echo >> $OUTPUT_FILE
done

echo
vasm6502_oldstyle -dotdir -Fbin $OUTPUT_FILE