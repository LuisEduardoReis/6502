
pattern_data_start    = $021c ; 16 bytes -> $022c
pattern_time          = $021c ; 2 bytes
                     ;= $021d
pattern_delay         = $021e ; 2 bytes
                     ;= $021f
pattern_select        = $0220
   
pattern_char          = $0221
pattern_index         = $0222
pattern_reset_request = $0223
pattern_value_A       = $0224  ; 2 bytes
                     ;= $0215
pattern_value_B       = $0226  ; 2 bytes
                     ;= $0227
pattern_value_C       = $0228  ; 2 bytes
                     ;= $0229
pattern_value_D       = $022a  ; 2 bytes
                     ;= $022b



reset_pattern:
  lda #0
  sta pattern_reset_request

  jsr clear_lcd

  lda uptime
  sta pattern_time
  lda uptime+1
  sta pattern_time+1
  
  lda pattern_select
  cmp #0
  beq reset_pattern_pattern_0
  cmp #1
  beq reset_pattern_pattern_1
  cmp #2
  beq reset_pattern_pattern_2
  cmp #3
  beq reset_pattern_pattern_3
  jmp return_reset_pattern

  reset_pattern_pattern_0:
    jsr pattern_0_reset
    jmp return_reset_pattern

  reset_pattern_pattern_1:
    jsr pattern_1_reset
    jmp return_reset_pattern

  reset_pattern_pattern_2:
    jsr pattern_2_reset
    jmp return_reset_pattern

  reset_pattern_pattern_3:
    jsr pattern_3_reset
    jmp return_reset_pattern

  return_reset_pattern:
  rts

; Update pattern - Called every tick to update the current pattern.
; Steps:
;   - Check for reset requests
;   - Check if timing is right for a new update
;   - Switch case through the available patterns
update_pattern:
  lda pattern_reset_request
  beq update_pattern_no_reset
  jsr reset_pattern
  update_pattern_no_reset:


  ;sei
  ldx uptime
  ldy uptime+1
  ;cli

  sec
  txa  
  sbc pattern_time
  cmp pattern_delay
  bcc return_update_pattern
  sec
  tya
  sbc pattern_time+1
  cmp pattern_delay+1
  bcc return_update_pattern

  lda uptime
  sta pattern_time
  lda uptime+1
  sta pattern_time+1

  ; Switch pattern
  ;sei
  lda pattern_select
  ;cli
  cmp #0
  beq update_pattern_pattern_0
  cmp #1
  beq update_pattern_pattern_1
  cmp #2
  beq update_pattern_pattern_2
  cmp #3
  beq update_pattern_pattern_3
  jmp return_update_pattern

  ; Patterns
  update_pattern_pattern_0:
    jsr pattern_0_character_listing
    jmp return_update_pattern

  update_pattern_pattern_1:
    jsr pattern_1_fibbonaci
    jmp return_update_pattern

  update_pattern_pattern_2:
    jsr pattern_2_ball
    jmp return_update_pattern

  update_pattern_pattern_3:
    jsr pattern_3_matrix
    jmp return_update_pattern

  return_update_pattern:
  rts



